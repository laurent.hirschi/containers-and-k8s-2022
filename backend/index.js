'use strict';
const express = require("express");
var bodyParser = require("body-parser");
const app = express();const mariadb = require("mariadb");
const JSONBig = require("json-bigint");

const pool = mariadb.createPool({
  host: "database",
  user: "root",
  password: "test",
  database: "DBConferences"
});

const port = 8080;

app.use(bodyParser.json());

// GET
app.get("/", (req, res) => {
  res.send("Hello World!");
});
// Probably remove this
app.get("/conference", async (req, res) => {
  let connection;
  try {
    connection = await pool.getConnection();
    console.log("connected");
    var rows = await connection.query("SELECT * from Conference");
    delete rows['meta'];
    res.send(rows);
    console.log(rows);
  } catch (error) {
    console.log(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
});
app.get 
app.get('/getConferencesById', async (req, res) => {
  let connection;
  try {
      connection = await pool.getConnection();
      let id = req.query.idMedecin;
      if (id) {
        var sqlInvitation = "SELECT PK_Conference, Medecin, Commercial, Titre, Lieu, "+
        "DateDebut, DateFin FROM `Invitation` INNER JOIN `Conference` ON Conference.PK_Conference=Invitation.Conference where Medecin=?";
        var sqlInscription = "SELECT PK_Conference, Medecin, Commercial, Titre, Lieu, "+
        "DateDebut, DateFin, DateInscription FROM `Inscription` INNER JOIN `Conference` ON Conference.PK_Conference=Inscription.Conference where Medecin=?";
        var rowsInvitations = await connection.query(sqlInvitation,id);
        var rowsInscriptions = await connection.query(sqlInscription,id);
        delete rowsInvitations['meta'];
        delete rowsInscriptions['meta'];
        var result = {};
        result['invitations'] = rowsInvitations;
        result['inscriptions'] = rowsInscriptions;
        console.log(result);
        res.send(result);
      }
  } catch (error) {
    console.log(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
});
/*
app.get('/select2', async (req, res) => {
  let connection;
  try {
      connection = await pool.getConnection();
      console.log("select2");
      let data = req.query.dataid;
      console.log("DATA: "+data)
      if (Number(data)) {
        var rows = await connection.query("SELECT * from Data where dataid = ?",data);
        delete rows['meta'];
        res.send(rows);
        console.log(rows);
      }
      
  } catch (error) {
      console.log(error);
  } finally {
      if (connection) {
      connection.release();
      }
  }
});
*/
app.get("/login", async (req, res) => {
  console.log("login");
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction
      // Create conference
      let email = req.query.username;
      let password = req.query.password;
      var resultTechnicien = await connection.query("SELECT * FROM Technicien WHERE email=?", [
        email,
      ]);
      delete resultTechnicien['meta'];
      var result;
      var type;
      if (resultTechnicien.length != 1) {
        var resultMedecin = await connection.query("SELECT * FROM Medecin WHERE email=?", [
          email,
        ]);
        delete resultMedecin['meta'];
        if (resultMedecin.length != 1) {
          var resultCommercial = await connection.query("SELECT * FROM Commercial WHERE email=?", [
            email,
          ]);
          delete resultCommercial['meta'];
          if (resultCommercial.length == 1) {
            result = resultCommercial[0];
            type = "UserRoles.commercial";
          }
        } else {
          result = resultMedecin[0];
          type = "UserRoles.medecin";
        }
      } else {
        result = resultTechnicien[0];
        type = "UserRoles.technicien";
      }
      var message;
      if (result) {
        if (result.Password === password) {
          message = {"result":"SUCCESS", "type":type}
        } else {
          message = {"result":"ERROR"}
        }
      } else {
        message = {"result":"ERROR"}
      }
      res.send(message);   
    } catch (err) {
      console.error("Error on request, reverting changes: ", err);
    }finally {
      if (connection) {
        connection.release();
      }
    }
});

app.get("/medecin", async (req, res) => {
  let connection;
  try {
    connection = await pool.getConnection();
    console.log("connected");
    rows = await connection.query("SELECT * from Medecin");
    res.send(rows);
    console.log(rows);
  } catch (error) {
    console.log(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
});


// POST addInvitation
app.post("/addInvitation", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    // Create invitation
    let conference = req.body.conference;
    let medecin = req.body.medecin;
    let commercial = req.body.commercial;

    var result = await connection.query(
      "INSERT INTO Invitation (Conference, Medecin, Commercial) VALUES (?,?,?)",
      [conference, medecin, commercial]
    );

    await connection.commit(); // Commit Changes
    resultAddConf = JSONBig.parse(JSONBig.stringify(result)).insertId;
    res.send({"result":"SUCCESS", "id":resultAddConf});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});


//POST
app.post("/addconference", async (req, res) => {
  try {
    connection = await pool.getConnection();
    await connection.beginTransaction(); // Start Transaction

    // Create conference
    let conference = req.body;
    console.log(conference);
    const result = await pool.query(
      "INSERT INTO `Conference` (`TechnicienValide`,`TechnicienResp`, `CommercialResp`, `Titre`, `Lieu`, `DateDebut`, `DateFin`, `NbMaxMedecin`) VALUES (?,?,?, ? ,?, ?, ? , 5)",
      [
        null,
        null,
        null, // For the moment null
        conference.Titre,
        conference.Lieu,
        conference.DateDebut,
        conference.DateFin,
      ]
    );
    
    resultAddConf = JSONBig.parse(JSONBig.stringify(result)).insertId;

    await connection.commit(); // Commit Changes
    res.send({"result":"SUCCESS", "id":resultAddConf});
  } catch (err) {
    console.error("Error on request, reverting changes: ", err);
    await connection.rollback(); // Rollback Changes
    res.status(400).send();
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

module.exports = {
  getConnection: function () {
    return new Promise(function (resolve, reject) {
      pool
        .getConnection()
        .then(function (connection) {
          resolve(connection);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  },
};
