-- CREATE DATABASE DBConferences;

USE DBConferences;

CREATE TABLE Technicien (
    Email Varchar (50) NOT NULL,
    Nom Varchar (50) NOT NULL,
    Prenom Varchar (50) NOT NULL,
    Password Varchar (50) NOT NULL,

    CONSTRAINT PK_Technicien PRIMARY KEY (Email)
);

CREATE TABLE Medecin (
    Email Varchar (50) NOT NULL,
    Nom Varchar (50) NOT NULL,
    Prenom Varchar (50) NOT NULL,
    Password Varchar (50) NOT NULL,

    CONSTRAINT PK_Medecin PRIMARY KEY (Email)
);

CREATE TABLE Commercial (
    Email Varchar (50) NOT NULL,
    Nom Varchar (50) NOT NULL,
    Prenom Varchar (50) NOT NULL,
    Password Varchar (50) NOT NULL,

    CONSTRAINT PK_Commercial PRIMARY KEY (Email)
);

CREATE TABLE Conference (
    PK_Conference Int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Titre Varchar (50) NOT NULL,
    Lieu Varchar (50) NULL,
    DateDebut Date NOT NULL,
    DateFin Date NOT NULL,
    NbMaxMedecin Int NOT NULL DEFAULT 5,
    TechnicienValide Varchar (50),
    TechnicienResp Varchar(50),
    CommercialResp Varchar(50),

    CONSTRAINT FK_TechnicienValide FOREIGN KEY (TechnicienValide) REFERENCES Technicien(Email),
    CONSTRAINT FK_TechnicienResp FOREIGN KEY (TechnicienResp) REFERENCES Technicien(Email),
    CONSTRAINT FK_CommercialResp FOREIGN KEY (CommercialResp) REFERENCES Commercial(Email),

    CONSTRAINT check_dates CHECK (DateDebut < DateFin)
);


CREATE TABLE Invitation (
    Medecin Varchar (50),
    Conference Int,
    Commercial Varchar (50) NOT NULL,

    CONSTRAINT PK_Invitation PRIMARY KEY (Medecin,Conference),
    CONSTRAINT FK_Commercial_Invitation FOREIGN KEY (Commercial) REFERENCES Commercial(Email),
    CONSTRAINT FK_Medecin_Invitation FOREIGN KEY (Medecin) REFERENCES Medecin(Email),
    CONSTRAINT FK_Conference_Invitation FOREIGN KEY (Conference) REFERENCES Conference(PK_Conference)
);

CREATE TABLE Inscription (
    Medecin Varchar (50),
    Conference Int,
    DateInscription Date NOT NULL,
    Commercial Varchar (50) NULL,

    CONSTRAINT PK_Inscription PRIMARY KEY (Medecin, Conference),
    CONSTRAINT FK_Commercial_Inscription FOREIGN KEY (Commercial) REFERENCES Commercial(Email),
    CONSTRAINT FK_Invitation FOREIGN KEY(Medecin, Conference) REFERENCES Invitation(Medecin, Conference)
);

-- Insert into Commercial 3 rows
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com1@com1.ch', 'Com1', 'Com1', 'com1');
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com2@com2.ch', 'Com2', 'Com2', 'com2');
INSERT INTO Commercial (Email, Nom, Prenom, Password) VALUES ('com3@com3.ch', 'Com3', 'Com3', 'com3');

-- Insert into Medecin 3 rows
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med1@med1.ch', 'Med1', 'Med1', 'med1');
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med2@med2.ch', 'Med2', 'Med2', 'med2');
INSERT INTO Medecin (Email, Nom, Prenom, Password) VALUES ('med3@med3.ch', 'Med3', 'Med3', 'med3');

-- Insert into Technicien 3 rows
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec1@tec1.ch', 'Tec1', 'Tec1', 'tec1');
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec2@tec2.ch', 'Tec2', 'Tec2', 'tec2');
INSERT INTO Technicien (Email, Nom, Prenom, Password) VALUES ('tec3@tec3.ch', 'Tec3', 'Tec3', 'tec3');

-- Insert into Conference 3 rows
INSERT INTO Conference (Titre, Lieu, DateDebut, DateFin, NbMaxMedecin, TechnicienValide, TechnicienResp, CommercialResp)
VALUES ('Conf1', 'Lieu1', '2019-01-01', '2020-01-02', 5, 'tec1@tec1.ch', 'tec1@tec1.ch', 'com1@com1.ch');
INSERT INTO Conference (Titre, Lieu, DateDebut, DateFin, NbMaxMedecin, TechnicienValide, TechnicienResp, CommercialResp)
VALUES ('Conf2', 'Lieu2', '2019-01-01', '2020-01-02', 5, 'tec2@tec2.ch', 'tec2@tec2.ch', 'com2@com2.ch');
INSERT INTO Conference (Titre, Lieu, DateDebut, DateFin, NbMaxMedecin, TechnicienValide, TechnicienResp, CommercialResp)
VALUES ('Conf2', 'Lieu2', '2019-01-01', '2020-01-02', 5, 'tec3@tec3.ch', 'tec3@tec3.ch', 'com3@com3.ch');

-- Insert into Invitation 3 rows
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med1@med1.ch', 1, 'com1@com1.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med2@med2.ch', 2, 'com2@com2.ch');
INSERT INTO Invitation (Medecin, Conference, Commercial) VALUES ('med3@med3.ch', 3, 'com3@com3.ch');

-- Insert into Inscirption 3 rows
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med1@med1.ch', 1, '2022-04-07', 'com1@com1.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med2@med2.ch', 2, '2022-04-07', 'com2@com2.ch');
INSERT INTO Inscription (Medecin, Conference, DateInscription, Commercial) VALUES ('med3@med3.ch', 3, '2022-04-07', 'com3@com3.ch');
